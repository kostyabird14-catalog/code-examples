<?php


namespace App\Iblocks;


use Bitrix\Main\Loader;
use CIBlock;
use CIBlockElement;
use CIBlockProperty;
use CIBlockPropertyEnum;
use CIBlockResult;
use CIBlockSection;
use Exception;
use ReflectionClass;

/**
 * Самописный класс для работы с инфоблоками на Bitrix
 */
abstract class BaseIblock
{
    protected static ?array $instance = null;

    protected ?int $iblock_id = null;
    protected ?string $iblock_code = null;
    protected ?array $iblock_properties = [];

    /**
     * private BaseBlock constructor. Use ::instance() instead
     * @throws Exception
     */
    private function __construct()
    {
        Loader::includeModule('iblock');
    }

    /**
     * @return static
     */
    public static function instance(): ?BaseIblock
    {
        if (!static::$instance[static::class]) {
            static::$instance[static::class] = new static();
        }

        return static::$instance[static::class];
    }

    /**
     * @return int
     * @throws Exception
     */
    public function id(): int
    {
        if (!$this->iblock_id) {
            $res = CIBlock::GetList(
                [
                    'SORT' => 'ASC'
                ],
                [
                    'CODE' => $this->code(),
                    '=ACTIVE' => 'Y',
                    'CHECK_PERMISSIONS' => 'N'
                ]
            );

            $iblock = $res->Fetch();

            $this->iblock_id = $iblock ? (int)$iblock['ID'] : null;
        }

        if (!$this->iblock_id) {
            throw new Exception('Iblock not found');
        }

        return $this->iblock_id;
    }

    /**
     * @return string
     */
    public function code(): string
    {
        if (!$this->iblock_code) {
            $function = new ReflectionClass(get_class($this));

            $this->iblock_code = static::getCustomIblockCode();

            if (!$this->iblock_code) {
                $classShortName = $function->getShortName();
                $classShortName = str_replace('Iblock', '', $classShortName);

                $this->iblock_code = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $classShortName));
            }
        }

        return $this->iblock_code;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getProperties(): array
    {
        if (!$this->iblock_properties) {
            $properties = [];

            $rs = CIBlockProperty::GetList([], ["IBLOCK_ID" => $this->id()]);
            while ($property = $rs->Fetch()) {
                $properties[$property['CODE']] = $property;
            }

            $this->iblock_properties = $properties;
        }

        return $this->iblock_properties;
    }

    /**
     * @return string|null
     */
    protected function getCustomIblockCode(): ?string
    {
        return null;
    }

    /**
     * @param int $id
     * @param array $select
     * @return array|null
     */
    public function getElementById(int $id, array $select = []): ?array
    {
        return $this->getElementList(['filter' => ['ID' => $id], 'select' => $select])->Fetch() ?: null;
    }

    /**
     * @param int $id
     * @param array $select
     * @return array|null
     * @throws Exception
     */
    public function getSectionById(int $id, array $select = []): ?array
    {
        return $this->getSectionList(['filter' => ['ID' => $id], 'select' => $select])->Fetch() ?: null;
    }

    /**
     * @param array $parameters
     * @return CIBlockResult
     * @throws Exception
     */
    public function getSectionList(array $parameters): CIBlockResult
    {
        $parameters['filter'] = $parameters['filter'] ?? [];

        $parameters['filter']['IBLOCK_ID'] = $this->id();

        if (!$parameters['select']) {
            $parameters['select'] = ['*', 'UF_*'];
        }

        $parameters['count'] = $parameters['count'] ?: false;

        return CIBlockSection::GetList(
            $parameters['order'],
            $parameters['filter'],
            $parameters['count'],
            $parameters['select'],
            $parameters['navigation']
        );
    }

    /**
     * @param array $parameters
     * @return CIBlockResult
     * @throws Exception
     */
    public function getElementList(array $parameters): CIBlockResult
    {
        $parameters['filter'] = $parameters['filter'] ?? [];

        $parameters['filter']['IBLOCK_ID'] = $this->id();

        if (!$parameters['select']) {
            $parameters['select'] = ['*'];

            foreach (array_keys($this->getProperties()) as $propertyCode) {
                $parameters['select'][] = 'PROPERTY_' . $propertyCode;
            }
        }

        return CIBlockElement::GetList(
            $parameters['order'] ?: [],
            $parameters['filter'],
            reset($parameters['group']) ?: false,
            $parameters['navigation'] ?: false,
            $parameters['select']
        );
    }

    public function getElementCount(array $filter = []): int
    {
        $filter['IBLOCK_ID'] = $this->id();

        return (int)(CIBlockElement::GetList([], $filter, false, [], ['ID'])->AffectedRowsCount());
    }

    /**
     * @param array $data
     * @return int
     * @throws Exception
     */
    public function addElement(array $data): int
    {
        $el = new CIBlockElement();

        $data['IBLOCK_ID'] = $this->id();

        return $el->Add($data);
    }

    /**
     * @param int $id
     * @param array $data
     * @return void
     * @throws Exception
     */
    public function updateElementProperties(int $id, array $data)
    {
        CIBlockElement::SetPropertyValuesEx($id, $this->id(), $data);
        CIBlock::clearIblockTagCache($this->id());
    }

    /**
     * @param int $id
     * @param array $data
     * @return bool|mixed
     */
    public function updateElementFull(int $id, array $data)
    {
        $el = new CIBlockElement;

        return $el->Update($id, $data);
    }

    /**
     * @param int $id
     * @return bool
     */
    public static function deleteElement(int $id): bool
    {
        return CIBlockElement::Delete($id);
    }

    /**
     * @param string $code
     * @return int|null
     * @throws Exception
     */
    public function getPropertyId(string $code): ?int
    {
        $properties = $this->getProperties();
        return $properties[$code] ? (int)$properties[$code]['ID'] : null;
    }

    /**
     * @param string $code
     * @param string $by
     * @return array|null
     * @throws Exception
     */
    public function getPropertyListValues(string $code, string $by = 'ID'): ?array
    {
        $propertyEnums = CIBlockPropertyEnum::GetList(
            [],
            ["IBLOCK_ID" => $this->id(), "CODE" => $code]
        );

        $items = [];
        while ($item = $propertyEnums->Fetch()) {
            $itemBy = $item[$by];
            $items[$itemBy] = [
                'ID' => $item['ID'],
                'NAME' => $item['VALUE'],
                'CODE' => $item['XML_ID'],
            ];
        }

        return $items;
    }
}