<?php

namespace App\Agents;

use CAgent;
use App\Actions\CatalogReport\SendEmailAction;
use App\Actions\CatalogReport\BuildDataTreeAction;
use App\Actions\CatalogReport\WriteFileAction;

final class CatalogReportAgent extends BaseAgent
{
    protected function runInstance()
    {
        set_time_limit(3600);

        $sections = BuildDataTreeAction::instance()->run();

        $fileLink = WriteFileAction::instance()
            ->setSections($sections)
            ->run();

        SendEmailAction::instance()
            ->setFileLink($fileLink)
            ->run();
    }

    protected function nextScript(): ?string
    {
        $agentName = parent::nextScript();
        $dateExec = date('d.m.Y', strtotime(' +1 day')) . ' 04:00:00'; // следующий день в 4 часа
        $agents = CAgent::GetList(
            array('ID' => 'DESC'),
            array('=NAME' => $agentName)
        );

        if ($id = $agents->Fetch()) {
            CAgent::Update(
                $id['ID'],
                [
                    'NEXT_EXEC' => $dateExec
                ]
            );
        } else {
            CAgent::AddAgent(
                $agentName,
                'main',
                'Y',
                '',
                '',
                'Y',
                $dateExec
            );
        }

        return $agentName;
    }
}