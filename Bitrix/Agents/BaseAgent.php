<?php

namespace App\Agents;

abstract class BaseAgent
{
    protected array $data = [];

    public static function run(?array $data = null): ?string
    {
        $self = (new static());

        $self->data = $data ?: [];
        $self->runInstance();

        return $self->nextScript();
    }

    protected function nextScript(): ?string
    {
        return '\\' . static::class . '::run();';
    }

    abstract protected function runInstance();
}