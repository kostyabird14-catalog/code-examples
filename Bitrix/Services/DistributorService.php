<?php

namespace App\Service;

use App\Helpers\RegexHelper;
use App\Traits\FunctionAsAttributeCallable;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;
use Bitrix\Sale\BasketItem;
use Carbon\Carbon;
use Carbon\Exceptions\InvalidFormatException;
use App\Entity;
use App\Exceptions\EntityIncompleteException;
use App\Exceptions\EntityNotFoundException;
use App\Exceptions\HttpClientException;
use App\Objectify\Distributor;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Collection;
use Illuminate\Translation\ArrayLoader;
use Illuminate\Translation\Translator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;

/**
 * @property int id
 * @property null|bool active
 * @property string name
 * @property string login
 * @property string code
 * @property string password
 * @property object data
 * @property null|bool is_second_delivery_price
 * @property null|object[] leftovers
 * @property null|object[] delivery
 * @property null|object[] deliveryOnOrder
 * @property null|object[] manager
 * @property int products_codes_count
 * @property null|bool leftovers_is_from_api
 * @property null|bool delivery_is_from_api
 * @property null|bool is_allow_to_pay_online
 *
 */
class DistributorService
{
    use FunctionAsAttributeCallable {
        __get as functionGet;
    }

    private Client $http;
    private object $distributor;

    private ?bool $leftoversIsFromApi = null;
    private ?bool $deliveryIsFromApi = null;
    private ?array $leftovers = null;
    private ?object $delivery = null;
    private ?object $deliveryOnOrder = null;
    private ?object $manager = null;

    /**
     *
     */
    public function __construct()
    {
        $this->http = new Client();
    }

    /**
     * @param $attribute
     * @return mixed
     */
    public function __get($attribute)
    {
        if (isset($this->distributor->$attribute)) {
            return $this->distributor->$attribute;
        }

        return $this->functionGet($attribute);
    }

    /**
     * @return static
     */
    private static function instance(): self
    {
        return new static();
    }

    /**
     * @param int $id
     * @return self
     * @throws ArgumentException
     * @throws EntityIncompleteException
     * @throws EntityNotFoundException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public static function fromId(int $id): self
    {
        /** @var Distributor $distributor */
        $distributor = Entity\DistributorsTable::getList(['filter' => ['ID' => $id, 'ACTIVE' => 1]])->fetchObject();

        if (!$distributor) {
            throw new EntityNotFoundException($id, Entity\DistributorsTable::class);
        }

        return static::fromObject($distributor);
    }

    /**
     * @param Distributor $distributor
     * @return DistributorService
     * @throws ArgumentException
     * @throws EntityIncompleteException
     */
    public static function fromObject(Distributor $distributor): DistributorService
    {
        $distributor = $distributor->formatted();

        if (!$distributor->data || !$distributor->data->delivery || !$distributor->data->payment) {

            throw new EntityIncompleteException($distributor->id, Entity\DistributorsTable::class);

        }

        $instance = static::instance();

        $instance->distributor = $distributor;

        return $instance;
    }

    /**
     * @return static[]
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public static function getAllDistributors(): array
    {
        $rs = Entity\DistributorsTable::getList(['filter' => ['ACTIVE' => 1]]);

        $distributors = [];

        while ($obDistributor = $rs->fetchObject()) {
            /** @var Distributor $obDistributor */
            try {
                $distributor = static::fromObject($obDistributor);
            } catch (EntityIncompleteException $e) {
                continue;
            }

            $distributors[] = $distributor;
        }

        return $distributors;
    }

    /**
     * @param string|null $url
     * @param array $formData
     * @return mixed
     * @throws HttpClientException
     */
    private function http(?string $url, array $formData)
    {
        if (!$url || !$formData) {
            throw new HttpClientException(HttpClientException::MESSAGE_BAD_REQUEST);
        }

        try {
            $response = $this->http->post($url, [
                'headers' => [
                    'Accept-Encoding' => 'gzip, deflate, br',
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                ],
                'auth' => [$this->distributor->login, $this->distributor->password],
                'form_params' => $formData,
                'timeout' => 5
            ]);
        } catch (GuzzleException $e) {
            throw new HttpClientException(HttpClientException::MESSAGE_BAD_RESPONSE);
        }

        if ($response->getStatusCode() !== 200) {
            throw new HttpClientException(HttpClientException::MESSAGE_CODE_NOT_200);
        }

        $response = (object)\json_decode($response->getBody()->getContents(), true);

        if (
            \JSON_ERROR_NONE !== \json_last_error() ||
            !($response->success ?? false) ||
            !($response->data ?? null)
        ) {
            throw new HttpClientException(HttpClientException::MESSAGE_INVALID_DATA);
        }

        return $response->data;
    }

    /**
     * @param string[] $codes Array products codes.
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     * @example $code = ['ABC', '012',];
     * @example return [['code' => 'ABC', 'quantity' => 1,], ['code' => '012', 'quantity' => 2,],]; # If param $associative === false
     */
    public function setLeftovers(array $codes)
    {
        $codes = array_filter(array_map(function ($code) {
            return is_scalar($code) ? (string)$code : null;
        }, $codes));

        if (!$codes) {
            return;
        }

        try {
            $data = $this->http(
                $this->distributor->method_quantity,
                [
                    'products' => $codes,
                ]
            );

            if (!is_array($data['products'])) {
                throw new HttpClientException();
            }

            $items = $data['products'];

            $this->leftoversIsFromApi = true;
        } catch (HttpClientException $e) {

            $items = Entity\LaravelCatalogQuantityTable::getList([
                'select' => [
                    'code',
                    'quantity',
                ],
                'filter' => [
                    'code' => $codes,
                    'distributor_id' => $this->distributor->id,
                ],
            ])->fetchAll();

            # CBitrix: remove `UALIAS_0`
            $items = array_map(function ($item) {
                if (isset($item['UALIAS_0'])) {
                    unset($item['UALIAS_0']);
                }
                return $item;
            }, $items);

            $this->leftoversIsFromApi = false;
        }

        $items = array_map(function ($item) {
            $item['quantity'] = $item['quantity'] ?? 0;
            $item['quantity'] = max((int)$item['quantity'], 0);

            return $item;
        }, $items);

        $this->leftovers = $items ?: [];
    }

    /**
     * @param array $data
     * @example $data = [
     *      'price' => 1234.56,
     *      'city' => 'Москва',
     *      'address' => 'Пресненская набережная, 8с1',
     *      'kladr_id' => '7700000000073880002',
     *      'coordinates' => [
     *          'latitude' => 55.747720,
     *          'longitude' => 37.538875,
     *      ],
     *      'products' => [
     *          [
     *              'code' => 'ABC',
     *              'price' => 78.9,
     *              'quantity' => 1,
     *          ],
     *      ],
     * ];
     */
    public function setDelivery(array $data)
    {
        $validator = $this->deliveryValidator($data);

        try {
            $validatedData = $validator->validate();

            $result = $this->http(
                $this->distributor->method_delivery,
                $validatedData
            );

            if (!isset($result['delivery']) || !isset($data['price'])) {
                throw new HttpClientException(HttpClientException::MESSAGE_INVALID_DATA);
            }

            $delivery = $result['delivery'];

            $this->deliveryIsFromApi = true;
        } catch (ValidationException|HttpClientException|InvalidFormatException $e) {

            $this->deliveryIsFromApi = false;
            $this->delivery = null;
            return;
        }

        $price = (float)$delivery['price'];

        $deliveriesList = $this->data->delivery;

        foreach ($deliveriesList as $deliveryItem) {
            if ($deliveryItem->type === $validatedData['type']) {
                if (
                    isset($deliveryItem->free_delivery_price)
                    && $deliveryItem->free_delivery_price < $validatedData['price']
                ) {
                    $price = 0;
                }
            }
        }

        $date = (string)$delivery['date'];
        $date = $date ? Carbon::createFromFormat('Y-m-d H:i:s', $date) : null;

        $this->delivery = (object)[
            'date' => $date,
            'price' => $price,
        ];
    }

    public function setDeliveryOnOrder(array $data)
    {
        $validator = $this->deliveryValidator($data);

        try {
            $validatedData = $validator->validate();

            $result = $this->http(
                $this->distributor->method_delivery,
                $validatedData
            );

            if (!isset($result['delivery']) || !isset($data['price'])) {
                throw new HttpClientException(HttpClientException::MESSAGE_INVALID_DATA);
            }

            $delivery = $result['delivery'];

        } catch (ValidationException|HttpClientException|InvalidFormatException $e) {

            $this->deliveryOnOrder = null;
            return;
        }

        $price = (float)$delivery['price'];

        $deliveriesList = $this->data->delivery;

        foreach ($deliveriesList as $deliveryItem) {
            if ($deliveryItem->type === $validatedData['type']) {
                if (
                    isset($deliveryItem->free_delivery_price)
                    && $deliveryItem->free_delivery_price < $validatedData['price']
                ) {
                    $price = 0;
                }
            }
        }

        $this->deliveryOnOrder = (object)[
            'price' => $price,
        ];
    }

    /**
     * @param int $orderId
     */
    public function setManager(int $orderId)
    {
        try {
            $manager = $this->http(
                    $this->distributor->method_manager,
                    [
                        'id' => $orderId
                    ]
                )['manager'] ?? null;
        } catch (HttpClientException $e) {
        }

        $this->manager = (object)[
            'full_name' => $manager['full_name'] ?? null,
            'phone' => $manager['phone'] ?? null,
            'email' => $manager['email'] ?? null,
        ];
    }

    /**
     * @param array $basketItemsByCodes
     * @return bool
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public function productsHasEnoughLeftovers(array $basketItemsByCodes): bool
    {
        if ($this->leftovers === null) {
            $this->setLeftovers(array_keys($basketItemsByCodes));
        }

        $distributorLeftovers = $this->leftovers;

        $distributorLeftovers = (new Collection($distributorLeftovers))->keyBy('code')->toArray();

        foreach ($basketItemsByCodes as $code => $basketItem) {
            /** @var BasketItem $basketItem */
            if (!isset($distributorLeftovers[$code]) || $distributorLeftovers[$code]['quantity'] < $basketItem->getQuantity()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param array $basketItemsByCodes
     * @return int
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public function productsEnoughLeftoversSum(array $basketItemsByCodes): int
    {
        if ($this->leftovers === null) {
            $this->setLeftovers(array_keys($basketItemsByCodes));
        }

        $distributorLeftovers = $this->leftovers;

        $distributorLeftovers = (new Collection($distributorLeftovers))->keyBy('code')->toArray();

        $enoughQuantityByCode = [];

        foreach ($basketItemsByCodes as $code => $basketItem) {
            /** @var BasketItem $basketItem */
            if (isset($distributorLeftovers[$code])) {
                $enoughQuantityByCode[$code] = $distributorLeftovers[$code]['quantity'] < $basketItem->getQuantity()
                    ? $distributorLeftovers[$code]['quantity']
                    : $basketItem->getQuantity();
            }
        }

        return array_sum($enoughQuantityByCode);
    }

    /**
     * @return array|null
     */
    public function getLeftovers(): ?array
    {
        return $this->leftovers;
    }

    /**
     * @return array|null
     */
    public function getDelivery(): ?object
    {
        return $this->delivery;
    }

    /**
     * @return array|null
     */
    public function getDeliveryOnOrder(): ?object
    {
        return $this->deliveryOnOrder;
    }

    /**
     * @return array|null
     */
    public function getLeftoversIsFromApi(): ?bool
    {
        return $this->leftoversIsFromApi;
    }

    /**
     * @return array|null
     */
    public function getDeliveryIsFromApi(): ?bool
    {
        return $this->deliveryIsFromApi;
    }

    /**
     * @return array|null
     */
    public function getManager(): ?object
    {
        return $this->manager;
    }

    /**
     * @return bool|null
     */
    private function getIsAllowToPayOnline(): ?bool
    {
        return $this->leftovers_is_from_api && $this->delivery_is_from_api;
    }

    /**
     * @return int
     */
    private function getProductsCodesCount(): int
    {
        return count(array_unique(array_column($this->leftovers, 'code')));
    }

    private function deliveryValidator($data): Validator
    {
        return new Validator(
            new Translator(new ArrayLoader(), 'ru'),
            $data,
            [
                'type' => [
                    'required',
                    'string',
                    Rule::in(array_column(b_config('digest.delivery.type'), 'id'))
                ],
                'price' => [
                    'required',
                    'numeric',
                    'min:0',
                ],
                'city' => [
                    'required',
                    'string',
                ],
                'address' => [
                    'required',
                    'string',
                ],
                'kladr_id' => [
                    'regex:' . RegexHelper::DIGITS,
                ],
                'coordinates' => [
                    'array',
                ],
                'coordinates.latitude' => [
                    'numeric',
                    'min:-90',
                    'max:90',
                ],
                'coordinates.longitude' => [
                    'numeric',
                    'min:-180',
                    'max:180',
                ],
                'products' => [
                    'required',
                    'array',
                ],
                'products.*.code' => [
                    'required',
                    'string',
                ],
                'products.*.price' => [
                    'required',
                    'numeric',
                    'min:0',
                ],
                'products.*.quantity' => [
                    'required',
                    'integer',
                    'min:1',
                ],
            ]
        );
    }

    public function paymentTypesFitsInPrice(float $orderPrice): array
    {
        $isFitsInMin = true;
        $isFitsInMax = true;

        $payments = $this->data->payment;
        $paymentTypes = [];

        foreach ($payments as $payment) {
            if ($payment->min) {
                $isFitsInMin = $orderPrice >= $payment->min;
            }

            if ($payment->max) {
                $isFitsInMax = $orderPrice <= $payment->max;
            }

            if ($isFitsInMin && $isFitsInMax) {
                $paymentTypes[] = $payment->type;
            }
        }

        return $paymentTypes;
    }

    public function paymentTypesAllowedByDelivery(?string $deliveryGuid): array
    {
        if (!$deliveryGuid) {
            return [];
        }

        $paymentTypes = [];

        foreach ($this->data->payment as $payment) {
            if (
                !isset($payment->prohibited_to) ||
                !is_array($payment->prohibited_to) ||
                !in_array($deliveryGuid, $payment->prohibited_to)
            ) {
                $paymentTypes[] = $payment->type;
            }
        }

        return $paymentTypes;
    }
}
