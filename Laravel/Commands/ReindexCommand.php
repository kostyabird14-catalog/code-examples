<?php

namespace App\Console\Commands\Elasticsearch;

use App\Services\ElasticService;
use Exception;
use Illuminate\Console\Command;

class ReindexCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'elasticsearch:reindex {--class=*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Indexes models to Elasticsearch';

    private ElasticService $service;

    public function __construct()
    {
        parent::__construct();

        $this->service = app(ElasticService::class);
        $this->service->setConsole($this);
    }

    /**
     * Execute the console command.
     *
     * @throws Exception
     */
    public function handle()
    {
        if (!config('services.elasticsearch.enabled')) {

            $this->info('Elasticsearch not enabled (see .env).');

        } else {

            $this->service->reindex($this->classes());

        }
    }

    /**
     * Get option '--class'
     *
     * @return null|array
     */
    protected function classes(): ?array
    {
        $classes = $this->option('class');

        if (!$classes) {
            return null;
        }

        if (!is_array($classes)) {
            $classes = explode(',', $classes);
        }

        return array_map(function ($class) {
            return 'App\\Models\\' . trim($class);
        }, $classes);
    }
}
