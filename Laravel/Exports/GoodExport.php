<?php


namespace App\Exports;


use App\Exports\Concerns\UseExistingFile;
use App\Models\Good;
use App\Models\Property;
use App\Models\Section;
use App\Services\GoodService;
use App\Services\Personal\SectionService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as BaseCollection;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

final class GoodExport implements UseExistingFile
{

    /**
     * @var Collection
     */
    protected Collection $goods;

    /**
     * @var GoodService
     */
    protected GoodService $goodService;

    /**
     * @var SectionService
     */
    protected SectionService $sectionService;

    /**
     * @var Spreadsheet Эталонный excel файл (заготовка), в который экспортируем товары.
     */
    protected Spreadsheet $spreadsheet;

    /**
     * @var Worksheet Лист "Данные о товарах", куда добавляем всю информацию по товарам.
     */
    protected Worksheet $goodsSheet;

    /**
     * @var Section|null Раздел, который экспортируем. null - если общий экспорт, без фильтра по разделу.
     */
    protected Section|null $section = null;

    /**
     * @var BaseCollection Коллекция всех свойств (Property) раздела, который выгружается.
     */
    protected BaseCollection $properties;

    /**
     * @var int С какой строки начинаются данные о товарах (номер строки первого товара).
     */
    protected int $goodsStartRowIndex = 5;

    /**
     * @var int В какой строке хранятся заголовки полей (Название, Описание, т.д.).
     */
    protected int $headerRowIndex = 3;

    /**
     * @var int В какой строке хранятся подсказки/описание полей для заполнение.
     */
    protected int $headerHintRowIndex = 4;

    /**
     * @var string В какой ячейке хранится название раздела, который выгружаем.
     */
    protected string $sectionTitleCell = 'B1';

    /**
     * @var string С какого столбца начинаются доп. свойства товаров.
     */
    protected string $propertiesStartColumnLetter = 'M';

    /**
     * @param GoodService $goodService
     * @param SectionService $sectionService
     */
    public function __construct(protected GoodService $goodService, protected SectionService $sectionService)
    {
    }

    /**
     * @param array $input
     * @return void
     */
    public function setInput(array $input = [])
    {
        $this->goods = $this->listGoods($input);

        $sectionId = $input['section'] ?? '';

        if ($sectionId) {
            $this->section = $this->sectionService->id($sectionId);
            $this->properties = $this->section->all_properties;
        } else {
            $this->properties = collect();
        }
    }

    /**
     * @return string
     */
    public function getExistingFilePath(): string
    {
        return Storage::path(config('catalog.goods_export_blank_file'));
    }

    /**
     * @param Spreadsheet $spreadsheet
     * @return void
     */
    public function spreadsheet(Spreadsheet $spreadsheet)
    {
        $this->spreadsheet = $spreadsheet;

        $this->goodsSheet = $this->spreadsheet->getSheet(2);

        $this->export();
    }

    /**
     * Выбрать товары по входным данным $input.
     * @param array $input
     * @return Collection
     */
    protected function listGoods(array $input = []): Collection
    {
        $_input = [
            'filter' => [
                'direct_section' => [$input['section'] ?? '']
            ]
        ];

        return $this->goodService->list($_input);
    }

    /**
     * @return $this
     */
    public function export(): static
    {
        $this->addSection();

        $this->addProperties();

        $this->addGoods();

        return $this;
    }

    /**
     * Добавить данные о выгружаемом разделе, если таковой выгружается.
     */
    protected function addSection(): void
    {
        if ($this->section) {
            $this->goodsSheet->setCellValue($this->sectionTitleCell, $this->section->title);
        }
    }

    /**
     * Добавить колонки (название) доп. свойств раздела.
     */
    protected function addProperties(): void
    {
        if ($this->properties->isNotEmpty()) {

            $iterator = $this->goodsSheet->getColumnIterator($this->propertiesStartColumnLetter);

            foreach ($this->section->all_properties as $property) {

                $coordinateTitle = $iterator->current()->getColumnIndex() . $this->headerRowIndex;
                $coordinateHint = $iterator->current()->getColumnIndex() . $this->headerHintRowIndex;

                $this->goodsSheet->setCellValue($coordinateTitle, $property->title);
                $this->goodsSheet->setCellValue($coordinateHint, $this->makePropertyHint($property));

                $iterator->next();
            }
        }
    }

    /**
     * @param Property $property
     * @return string
     */
    protected function makePropertyHint(Property $property): string
    {
        return collect([
            $property->required ? config('catalog.goods_import.hint_required_property') : '',
            $property->description ?? ''
        ])
            ->filter()
            ->implode('. ');
    }

    /**
     * Добавить данные о товарах.
     */
    protected function addGoods(): void
    {
        $rowIndex = $this->goodsStartRowIndex;

        foreach ($this->goods as $good) {

            $this->addGood($good, $rowIndex);

            $rowIndex++;
        }
    }

    /**
     * @param Good $good
     * @param int $rowIndex
     */
    protected function addGood(Good $good, int $rowIndex): void
    {
        $columns = [
            'title',
            'brand',
            'image',
            'description',
            'article',
            'barcode',
            'images',
            'section',
            'weight',
            'width',
            'height',
            'length',
        ];

        $goodRow = [];

        foreach ($columns as $columnName) {

            $value = match ($columnName) {
                'brand' => $good->brand->title,
                'section' => $good->section->title,
                'images' => implode(';', $good->images ?? []),
                default => $good->$columnName
            };

            $goodRow[] = $value;
        }

        foreach ($this->properties as $property) {

            $value = $good->text_properties[$property->id] ?? '';

            if (is_array($value)) {

                sort($value);
                $value = implode(',', $value);
            }

            $goodRow[] = $value;
        }

        $this->addRow($this->goodsSheet, $goodRow, $rowIndex);
    }

    /**
     * Добавить массив данных в нужную строку листа, начиная со столбца $columnLetter. <br>
     * eq.: addRow($sheet, [1,2,'test'], 5, 'A')
     * @param Worksheet $sheet
     * @param array $data
     * @param int $rowIndex
     * @param string $columnLetter
     */
    protected function addRow(Worksheet $sheet, array $data, int $rowIndex, string $columnLetter = 'A')
    {
        $iterator = $this->goodsSheet->getColumnIterator($columnLetter);

        foreach ($data as $value) {

            $coordinate = $iterator->current()->getColumnIndex() . $rowIndex;

            $sheet->setCellValue($coordinate, $value);

            $sheet->getCell($coordinate)->getStyle()->applyFromArray($this->getGoodCellStyle());

            $iterator->next();
        }

        $sheet->getRowDimension($rowIndex)->setRowHeight(30);
    }

    /**
     * Получить массив стилей для ячеек с данными товара.
     * @return array
     */
    protected function getGoodCellStyle(): array
    {
        return [
            'font' => [
                'name' => 'Calibri',
                'size' => 11
            ],
            'fill' => [
                "endColor" => [
                    "argb" => "FFFFFFFF"
                ],
                "fillType" => "solid",
                'startColor' => [
                    'argb' => 'FFFFCC99'
                ]
            ],
            'borders' => [
                'bottom' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
                'left' => [
                    'borderStyle' => Border::BORDER_DASHED,
                ],
                'right' => [
                    'borderStyle' => Border::BORDER_DASHED,
                ],

            ]
        ];
    }

}
