<?php


namespace App\Exports;


use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

abstract class BaseExport implements FromCollection, WithStyles, WithColumnWidths
{
    protected array $columns;
    protected int $listRow = 3;
    protected Collection $collection;

    /**
     * BaseExport constructor.
     */
    public function __construct()
    {
        $this->collection = collect();
    }

    /**
     * @param $value
     */
    protected function addMainTitle($value)
    {
        $this->collection->put(0, [$value]);
    }

    /**
     * @param $values
     */
    protected function addColumnTitles($values)
    {
        $row = [];

        foreach ($this->columns as $columnName => $column) {
            $row[$column] = $values[$columnName] ?? '';
        }

        $this->collection->put(1, $row);
    }

    /**
     * @param array $values
     */
    protected function add(array $values)
    {
        $row = [];

        foreach ($this->columns as $columnName => $column) {
            $row[$column] = $values[$columnName] ?? '';
        }

        $this->collection->put($this->listRow, $row);

        $this->listRow++;
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        return $this->collection->sortKeys();
    }

    /**
     * @return array
     */
    abstract public function columnWidths(): array;

    /**
     * @param Worksheet $sheet
     * @return mixed
     */
    abstract public function styles(Worksheet $sheet): mixed;
}
